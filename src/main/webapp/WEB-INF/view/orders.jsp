<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="UTF-8">
    <title>Orders</title>
</head>
<body>
    <h1 style="text-align: center">Orders</h1>

    <h2>All orders</h2>
    <div>
        <ul>
            <c:forEach var="o" items="${requestScope.get('orders')}">
                <li>
                    ${o.toString()}
                    <input type="button" class="delete__btn" data-order-id="${o.getId()}" value="Delete" name="Delete">
                </li>
            </c:forEach>
        </ul>
    </div>

    <h2>Add new order</h2>
    <form action="" method="post">
        <c:forEach var="pt" items="${requestScope.get('pizzaTypes')}">
            <label for="${pt.toString()}">${pt.toString()}</label>
            <input type="checkbox" id="${pt.toString()}" name="pizzaType" value="${pt.toString()}"><br>
            Amount: <input type="text" name="${pt.toString().concat('amount')}"><br>
        </c:forEach>
        <input type="submit" name="Order" value="Order">
    </form>

    <script>
        let deleteButtons = document.getElementsByClassName("delete__btn");
        for (let b of deleteButtons) {
            b.addEventListener("click", e => {
                fetch("/api/orders/" + b.getAttribute("data-order-id"), {
                    method: "DELETE"
                }).then(() => {
                    location.reload();
                });
            });
        }
    </script>
</body>
</html>
