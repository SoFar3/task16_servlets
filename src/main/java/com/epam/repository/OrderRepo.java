package com.epam.repository;

import com.epam.model.Order;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class OrderRepo {

    private List<Order> orders;

    private static OrderRepo instance;

    private OrderRepo() {
        orders = new CopyOnWriteArrayList<>();
    }

    public static OrderRepo getInstance() {
        if (instance == null) {
            synchronized (OrderRepo.class) {
                if (instance == null) {
                    instance = new OrderRepo();
                }
            }
        }
        return instance;
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

    public synchronized void deleteOrder(Integer id) {
        for (int i = orders.size() - 1; i >= 0; i--) {
            Order order = orders.get(i);
            if (order.getId().equals(id)) {
                orders.remove(order);
                break;
            }
        }
    }

    public Order getOrder(Integer id) {
        return orders
                .stream()
                .filter(o -> o.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Order> getOrders() {
        return orders;
    }

}
