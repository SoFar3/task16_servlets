package com.epam.model;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Order {

    private volatile static AtomicInteger AUTO_INCREMENT_ID = new AtomicInteger();

    private Integer id;
    private Map<Pizza, Integer> pizzas;

    public Order() {
        pizzas = new LinkedHashMap<>();
        id = AUTO_INCREMENT_ID.getAndIncrement();
    }

    public void addPizza(Pizza pizza, Integer amount) {
        pizzas.put(pizza, amount);
    }

    public Map<Pizza, Integer> getPizzas() {
        return pizzas;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "\n\tid=" + id +
                "\n\tpizzas=" + pizzas +
                "\n}";
    }

}
