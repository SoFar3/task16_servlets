package com.epam.model;

public class Pizza {

    private PizzaType type;
    private Recipe recipe;

    public Pizza() {
    }

    public Pizza(PizzaType type, Recipe recipe) {
        this.type = type;
        this.recipe = recipe;
    }

    public PizzaType getType() {
        return type;
    }

    public void setType(PizzaType type) {
        this.type = type;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "\n\ttype=" + type +
                "\n\trecipe=" + recipe +
                "\n}";
    }

}
