package com.epam.model;

import java.util.List;

public class Recipe {

    private List<Topping> toppings;
    private Dough dough;
    private Sauce sauce;

    public Recipe() {
    }

    public Recipe(List<Topping> toppings, Dough dough, Sauce sauce) {
        this.toppings = toppings;
        this.dough = dough;
        this.sauce = sauce;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public void setToppings(List<Topping> toppings) {
        this.toppings = toppings;
    }

    public Dough getDough() {
        return dough;
    }

    public void setDough(Dough dough) {
        this.dough = dough;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "toppings=" + toppings +
                ", dough=" + dough +
                ", sauce=" + sauce +
                '}';
    }

}
