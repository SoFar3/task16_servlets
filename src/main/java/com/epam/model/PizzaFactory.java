package com.epam.model;

public class PizzaFactory {

    private static PizzaFactory instance;
    private Bakery bakery;

    private PizzaFactory() {
        this.bakery = new Bakery();
    }

    public static PizzaFactory getInstance() {
        if (instance == null) {
            instance = new PizzaFactory();
        }
        return instance;
    }

    public Pizza createPizza(PizzaType type) {
        Pizza pizza = new Pizza();
        pizza.setType(type);
        pizza.setRecipe(bakery.getRecipe(type));
        return pizza;
    }

}
