package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OrdersFilter implements Filter {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        String servletPath = req.getServletPath();

        if ( ! servletPath.equals("/api/orders")) {
            String originServletPath = servletPath
                    .replace(
                            servletPath.substring(servletPath.lastIndexOf("/")), "");

            String pathVariable = req.getServletPath().replace("/api/orders/", "");

            try {
                int orderId = Integer.parseInt(pathVariable);
                req.setAttribute("orderId", orderId);
                req.getRequestDispatcher(originServletPath).forward(req, res);
                return;
            } catch (NumberFormatException e) {
                res.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
        }

        filterChain.doFilter(req, res);
    }

}
