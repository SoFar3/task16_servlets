package com.epam;

import com.epam.model.Order;
import com.epam.model.Pizza;
import com.epam.model.PizzaFactory;
import com.epam.model.PizzaType;
import com.epam.repository.OrderRepo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OrdersServlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger();

    private OrderRepo orderRepo;
    private PizzaFactory pizzaFactory;

    @Override
    public void init() {
        orderRepo = OrderRepo.getInstance();
        pizzaFactory = PizzaFactory.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer orderId = (Integer)req.getAttribute("orderId");
        if (orderId == null) {
            req.setAttribute("pizzaTypes", PizzaType.values());
            req.setAttribute("orders", orderRepo.getOrders());

            req.getRequestDispatcher("/WEB-INF/view/orders.jsp").forward(req, resp);
            return;
        }

        resp.setContentType("text/html; charset=UTF=8");
        ServletOutputStream outputStream = resp.getOutputStream();

        outputStream.println(orderRepo.getOrder(orderId).toString());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] pizzaTypes = req.getParameterValues("pizzaType");
        Order order = new Order();
        for (String value : pizzaTypes) {
            Integer amount = Integer.parseInt(req.getParameter(value + "amount"));
            PizzaType pizzaTypeOrigin = PizzaType.valueOf(value);
            Pizza pizza = pizzaFactory.createPizza(pizzaTypeOrigin);

            order.addPizza(pizza, amount);
        }
        orderRepo.addOrder(order);
        resp.sendRedirect("/api/orders");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer orderId = (Integer)req.getAttribute("orderId");
        if (orderId == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        orderRepo.deleteOrder(orderId);
        resp.sendRedirect("/api/orders");
    }

}
